<?php

namespace App\Providers;

use Parsedown;

class MarkdownWrapper
{
    /**
     * @param $text
     * @return string
     */
    public function toHTML($text)
    {
        return (new Parsedown())->text($text);
    }

    protected function preTransformText($text)
    {
        return $text;
    }

    protected function postTransformText($text)
    {
        return $text;
    }
}
