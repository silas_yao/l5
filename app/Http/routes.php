<?php

/*
|--------------------------------------------------------------------------
| Blog pages
|--------------------------------------------------------------------------
*/
get('/', function () {
    return view('welcome1');
    //    return redirect('/blog');
});

Route::get('blog', 'BlogController@index');
Route::get('blog/{slug}', 'BlogController@showPost');

/*
|--------------------------------------------------------------------------
| Admin area
|--------------------------------------------------------------------------
*/
Route::get('admin', function () {
    return redirect('/admin/post');
});

Route::group([
    'namespace' => 'Admin',
    'middleware' => 'auth',
    'prefix' => 'admin'
], function () {
    resource('post', 'PostController');
    resource('tag', 'TagController');
    get('upload', 'UploadController@index');
});

/*
|--------------------------------------------------------------------------
| Log in and out
|--------------------------------------------------------------------------
*/
Route::get('/auth/login','Auth\AuthController@getLogin');
Route::post('/auth/login','Auth\AuthController@postLogin');
Route::get('/auth/logout','Auth\AuthController@getLogout');