<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->freeEmail,
        'password' => bcrypt('qwerty'),
//        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Post::class, function () {
    $faker = Faker\Factory::create('zh_CN');
    return [
        'title' => $faker->lastName . $faker->firstName . $faker->address . $faker->numerify('640#######'),
        //        'title' => $faker->sentence(mt_rand(3,10)),
//        'content' => $faker->realText(),
        'content' => join("\n",$faker->paragraphs(mt_rand(3,6))),
        'created_at' => $faker->dateTimeBetween('-2 months', '+3 days'),
        'updated_at' => $faker->dateTimeBetween('-2 months', '+3 days'),
        'published_at' => $faker->dateTimeBetween('-2 months', '+3 days'),
    ];
});
